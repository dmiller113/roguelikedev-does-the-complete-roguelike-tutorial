module Utility.Map exposing (floorMap, prefabMap, testMap)

import Constant.Game as Game
import Constant.Map exposing (testMapLayout)
import Data.GameTile exposing (GameTile(..), fromString)
import Data.Map exposing (TileMap, addTile, emptyTileMap)
import Data.Position exposing (Position, fromIndex)
import List


floorMap : TileMap
floorMap =
    let
        foldBy : Int -> TileMap -> TileMap
        foldBy index acc =
            let
                tileAdd : Position -> TileMap
                tileAdd =
                    addTile acc CaveFloor
            in
            tileAdd <| fromIndex index
    in
    List.foldl foldBy emptyTileMap Game.mapIndexList


testMap : TileMap
testMap =
    let
        foldBy : Int -> TileMap -> TileMap
        foldBy index acc =
            let
                ( xPos, yPos ) =
                    fromIndex index

                tile =
                    if xPos > 24 || yPos > 6 || xPos == 0 || yPos == 0 then
                        CaveNSWall

                    else
                        CaveFloor

                tileAdd : Position -> TileMap
                tileAdd =
                    addTile acc tile
            in
            tileAdd ( xPos, yPos )
    in
    List.foldl foldBy emptyTileMap Game.mapIndexList


prefabMap : TileMap
prefabMap =
    let
        foldWithIndex_ : (Int -> Char -> a -> a) -> a -> Int -> String -> a
        foldWithIndex_ fn acc index string =
            case String.uncons string of
                Nothing ->
                    acc

                Just ( x, xs ) ->
                    foldWithIndex_ fn (fn index x acc) (index + 1) xs

        foldWithIndex : (Int -> Char -> a -> a) -> a -> String -> a
        foldWithIndex f default str =
            foldWithIndex_ f default 0 str

        foldBy index char acc =
            let
                pos =
                    fromIndex index

                mGameTile =
                    String.fromChar char |> fromString
            in
            case mGameTile of
                Nothing ->
                    acc

                Just gt ->
                    addTile acc gt pos
    in
    foldWithIndex foldBy emptyTileMap testMapLayout
