module Utility.Game exposing (flip, foldGameReducers)


flip : (a -> b -> c) -> (b -> a -> c)
flip fn =
    \b a -> fn a b


foldGameReducers :
    msg
    -> (msg -> state -> ( state, Cmd msg ))
    -> ( state, Cmd msg )
    -> ( state, Cmd msg )
foldGameReducers msg reducer ( oldState, oldCmd ) =
    let
        ( newState, newCmd ) =
            reducer msg oldState
    in
    ( newState, Cmd.batch [ oldCmd, newCmd ] )
