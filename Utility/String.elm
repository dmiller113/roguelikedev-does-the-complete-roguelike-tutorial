module Utility.String exposing (parens)

import String exposing (fromInt)


parens : Int -> String
parens v =
    "(" ++ fromInt v ++ ")"
