module View.Glyph exposing (glyph)

import Data.Color exposing (colorToHex)
import Data.GameTile exposing (tileFromGameTile)
import Data.Glyph exposing (Glyph)
import String exposing (fromInt)
import Svg exposing (text, text_)
import Svg.Attributes
    exposing
        ( class
        , dominantBaseline
        , fill
        , x
        , y
        )
import Update.Msg exposing (Msg)


glyph : Glyph -> Svg.Svg Msg
glyph { position, tile } =
    let
        ( px, py ) =
            position

        drawTile =
            tileFromGameTile tile
    in
    text_
        [ dominantBaseline "text-before-edge"
        , x <| fromInt <| px * 10
        , y <| fromInt <| py * 10
        , class "small"
        , fill <| colorToHex drawTile.color
        ]
        [ text drawTile.s ]
