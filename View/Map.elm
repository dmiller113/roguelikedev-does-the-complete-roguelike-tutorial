module View.Map exposing (gameMap)

import Constant.Game exposing (mapStartGlyph, playerID)
import Data.Entity exposing (Entity)
import Data.GameTile exposing (GameTile, isSeeThrough)
import Data.Glyph exposing (Glyph, fromGameTile)
import Data.Map exposing (EntityMap, GameMap, TileMap, whereIs)
import Data.Position exposing (Position, neighbors)
import Data.RogueState exposing (RogueState)
import Dict
import Set exposing (Set)
import Svg exposing (Svg)
import Update.Msg exposing (Msg)
import Utility.Game exposing (flip)
import View.Glyph exposing (glyph)


translateGlyphToMapGlyph : Glyph -> Glyph
translateGlyphToMapGlyph sigil =
    let
        { position } =
            sigil

        ( x, y ) =
            position
    in
    { sigil
        | position = ( x, y + mapStartGlyph )
    }


getTile : Position -> TileMap -> Maybe GameTile
getTile =
    Dict.get


getEntity : Position -> EntityMap -> Maybe Entity
getEntity =
    Dict.get


isPositionSeeThrough : Position -> TileMap -> Maybe Bool
isPositionSeeThrough pos tile =
    getTile pos tile |> Maybe.map isSeeThrough


viableNeighborPositions : TileMap -> Position -> Set Position -> Set Position
viableNeighborPositions m pos acc =
    case isPositionSeeThrough pos m of
        Nothing ->
            acc

        Just False ->
            acc

        Just True ->
            let
                neighborPositions =
                    Set.fromList <|
                        neighbors pos
            in
            Set.union acc neighborPositions


floodFill : GameMap -> Position -> Set Position
floodFill { tileMap } pos =
    let
        initialSet =
            Set.fromList <| pos :: neighbors pos

        floodFill_ : Set Position -> Set Position -> Set Position
        floodFill_ acc newPositions =
            if Set.isEmpty newPositions then
                acc

            else
                let
                    newAcc =
                        Set.union acc newPositions

                    goodNewPositions =
                        Set.diff newPositions acc
                            |> Set.foldl
                                (viableNeighborPositions tileMap)
                                Set.empty
                in
                floodFill_ newAcc goodNewPositions
    in
    floodFill_ Set.empty initialSet


positionToGlyphs : TileMap -> Position -> Maybe Glyph
positionToGlyphs m pos =
    let
        tile =
            getTile pos m

        mapper =
            flip fromGameTile pos >> translateGlyphToMapGlyph
    in
    Maybe.map mapper tile


positionSetToSvg : GameMap -> Position -> List (Svg Msg) -> List (Svg Msg)
positionSetToSvg map pos acc =
    let
        { entityMap, tileMap } =
            map
    in
    case getEntity pos entityMap of
        Nothing ->
            case Maybe.map glyph <| positionToGlyphs tileMap pos of
                Nothing ->
                    acc

                Just sigil ->
                    sigil :: acc

        Just _ ->
            (glyph <|
                translateGlyphToMapGlyph
                    { tile = Data.GameTile.Player
                    , position = pos
                    }
            )
                :: acc


gameMap : RogueState -> List (Svg Msg)
gameMap state =
    let
        playerPosition =
            whereIs state.gameMap playerID

        goodPositions =
            Maybe.map
                (floodFill state.gameMap)
                playerPosition
    in
    case goodPositions of
        Nothing ->
            []

        Just positions ->
            Set.foldl
                (positionSetToSvg state.gameMap)
                []
                positions
