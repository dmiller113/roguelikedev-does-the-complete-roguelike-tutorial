module View.StatusBar exposing (view)

import Constant.Game
    exposing
        ( statusColor
        , statusRow
        )
import Data.Aurum exposing (Aurum)
import Data.Experience
    exposing
        ( Experience
        , experienceToLevel
        )
import Data.Map exposing (GameMap)
import Data.RogueState exposing (RogueState)
import Data.Stremf exposing (strengthToString)
import String exposing (fromInt)
import Svg exposing (Attribute, Svg, text, text_)
import Svg.Attributes
    exposing
        ( class
        , dominantBaseline
        , fill
        , x
        , y
        )
import Update.Msg exposing (Msg)


attributes : List (Attribute Msg)
attributes =
    [ dominantBaseline "text-before-edge"
    , x <| fromInt <| 0
    , y <| fromInt <| statusRow * 10
    , class "small"
    , fill statusColor
    ]


auString : Aurum -> String
auString au =
    "Au:" ++ fromInt au ++ "  "


levelToString : GameMap -> String
levelToString { dlevel } =
    "Lev:" ++ fromInt dlevel ++ "  "


xpToString : Experience -> String
xpToString =
    fromInt


expLevelToString : Experience -> String
expLevelToString =
    experienceToLevel >> fromInt


experienceToString : Experience -> String
experienceToString xp =
    "Exp:"
        ++ expLevelToString xp
        ++ "/"
        ++ xpToString xp


rogueToString : RogueState -> String
rogueToString rogue =
    let
        { name, gameMap, experience, au, strength } =
            rogue

        nameString =
            name ++ "  "

        levelString =
            levelToString gameMap

        hpString =
            "HP:" ++ "10" ++ "(" ++ "12" ++ ")" ++ "  "

        strengthString =
            strengthToString strength

        goldString =
            auString au

        armorString =
            "Arm:" ++ "XX" ++ "  "

        experienceString =
            experienceToString experience
    in
    nameString
        ++ levelString
        ++ hpString
        ++ strengthString
        ++ goldString
        ++ armorString
        ++ experienceString


view : RogueState -> Svg Msg
view rogue =
    text_ attributes
        [ text <| rogueToString rogue ]
