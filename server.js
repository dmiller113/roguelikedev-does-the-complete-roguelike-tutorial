const express = require('express');
const app = express();

app.use(express.static('public'));
app.use(express.static('fonts'));

console.log('listening on port: 4000');
app.listen(4000, '0.0.0.0')
