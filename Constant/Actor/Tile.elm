module Constant.Actor.Tile exposing
    ( ant
    , bat
    , centar
    , dragon
    , floatingEye
    , griffin
    , hobgoblin
    , invisibleStalker
    , jackal
    , kobold
    , leprechaun
    , mimic
    , nymph
    , orc
    , player
    , purpleWorm
    , quasit
    , rustMonster
    , snake
    , troll
    , umberHulk
    , vampire
    , venusFlytrap
    , wraith
    , xorn
    , yeti
    , zombie
    )

import Data.Color
    exposing
        ( Color(..)
        , ColorCode(..)
        , ColorIntensity(..)
        )
import Data.Tile exposing (Tile)


player : Tile
player =
    { s = "@"
    , color = Color White Bold
    }


ant : Tile
ant =
    { s = "A"
    , color = Color White Bold
    }


bat : Tile
bat =
    { s = "B"
    , color = Color White Bold
    }


centar : Tile
centar =
    { s = "C"
    , color = Color White Bold
    }


dragon : Tile
dragon =
    { s = "D"
    , color = Color White Bold
    }


floatingEye : Tile
floatingEye =
    { s = "E"
    , color = Color White Bold
    }


venusFlytrap : Tile
venusFlytrap =
    { s = "F"
    , color = Color White Bold
    }


griffin : Tile
griffin =
    { s = "G"
    , color = Color White Bold
    }


hobgoblin : Tile
hobgoblin =
    { s = "H"
    , color = Color White Bold
    }


invisibleStalker : Tile
invisibleStalker =
    { s = "I"
    , color = Color White Bold
    }


jackal : Tile
jackal =
    { s = "J"
    , color = Color White Bold
    }


kobold : Tile
kobold =
    { s = "K"
    , color = Color White Bold
    }


leprechaun : Tile
leprechaun =
    { s = "L"
    , color = Color White Bold
    }


mimic : Tile
mimic =
    { s = "M"
    , color = Color White Bold
    }


nymph : Tile
nymph =
    { s = "N"
    , color = Color White Bold
    }


orc : Tile
orc =
    { s = "O"
    , color = Color White Bold
    }


purpleWorm : Tile
purpleWorm =
    { s = "P"
    , color = Color White Bold
    }


quasit : Tile
quasit =
    { s = "Q"
    , color = Color White Bold
    }


rustMonster : Tile
rustMonster =
    { s = "R"
    , color = Color White Bold
    }



-- snek


snake : Tile
snake =
    { s = "S"
    , color = Color White Bold
    }


troll : Tile
troll =
    { s = "T"
    , color = Color White Bold
    }


umberHulk : Tile
umberHulk =
    { s = "U"
    , color = Color White Bold
    }


vampire : Tile
vampire =
    { s = "V"
    , color = Color White Bold
    }


wraith : Tile
wraith =
    { s = "W"
    , color = Color White Bold
    }


xorn : Tile
xorn =
    { s = "X"
    , color = Color White Bold
    }


yeti : Tile
yeti =
    { s = "Y"
    , color = Color White Bold
    }


zombie : Tile
zombie =
    { s = "Z"
    , color = Color White Bold
    }
