module Constant.Game exposing
    ( glyphHeightPx
    , glyphWidthPx
    , mapHeightGlyph
    , mapIndexList
    , mapLastHeightIndex
    , mapLastWidthIndex
    , mapStartGlyph
    , mapWidthGlyph
    , messageRow
    , playerID
    , roomMaxHeight
    , roomMaxWidth
    , screenHeightGlyph
    , screenHeightPx
    , screenWidthGlyph
    , screenWidthPx
    , statusColor
    , statusRow
    , svgHeightPx
    , svgWidthPx
    , viewBoxDimensions
    )

import Data.Entity exposing (Entity)
import String exposing (fromInt)


playerID : Entity
playerID =
    -1


glyphWidthPx : Int
glyphWidthPx =
    10


glyphHeightPx : Int
glyphHeightPx =
    10


screenWidthGlyph : Int
screenWidthGlyph =
    80


screenHeightGlyph : Int
screenHeightGlyph =
    24


mapHeightGlyph : Int
mapHeightGlyph =
    22


mapLastHeightIndex : Int
mapLastHeightIndex =
    mapHeightGlyph - 1


mapLastWidthIndex : Int
mapLastWidthIndex =
    mapWidthGlyph - 1


mapWidthGlyph : Int
mapWidthGlyph =
    80


mapStartGlyph : Int
mapStartGlyph =
    1


mapIndexList : List Int
mapIndexList =
    List.range 0 (mapWidthGlyph * mapHeightGlyph - 1)


messageRow : Int
messageRow =
    0


roomMaxHeight : Int
roomMaxHeight =
    5


roomMaxWidth : Int
roomMaxWidth =
    23


statusColor : String
statusColor =
    "white"


statusRow : Int
statusRow =
    23


screenWidthPx : Int
screenWidthPx =
    screenWidthGlyph * glyphWidthPx


screenHeightPx : Int
screenHeightPx =
    screenHeightGlyph * glyphHeightPx


viewBoxDimensions : String
viewBoxDimensions =
    "0 0"
        ++ " "
        ++ fromInt screenWidthPx
        ++ " "
        ++ fromInt screenHeightPx


svgWidthPx : String
svgWidthPx =
    "900"


svgHeightPx : String
svgHeightPx =
    "540"



-- Original size: 270
