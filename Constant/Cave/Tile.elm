module Constant.Cave.Tile exposing
    ( caveCorridor
    , caveDoor
    , caveFloor
    , caveNSWall
    , caveWEWall
    )

import Constant.Cave.Glyph as Glyph
import Data.Color
    exposing
        ( Color(..)
        , ColorCode(..)
        , ColorIntensity(..)
        )
import Data.Tile exposing (Tile)


caveFloor : Tile
caveFloor =
    { s = Glyph.caveFloor
    , color = Color Green Bold
    }


caveNSWall : Tile
caveNSWall =
    { s = Glyph.caveNSWall
    , color = Color Yellow Regular
    }


caveWEWall : Tile
caveWEWall =
    { s = Glyph.caveWEWall
    , color = Color Yellow Regular
    }


caveDoor : Tile
caveDoor =
    { s = Glyph.caveDoor
    , color = Color Yellow Regular
    }


caveCorridor : Tile
caveCorridor =
    { s = Glyph.caveCorridor
    , color = Color White Regular
    }
