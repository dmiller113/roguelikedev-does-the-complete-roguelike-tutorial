module Constant.Cave.Glyph exposing
    ( caveCorridor
    , caveDoor
    , caveFloor
    , caveFloorDarkDisplay
    , caveFloorDarkRead
    , caveNSWall
    , caveWEWall
    )


caveFloor : String
caveFloor =
    "."


caveFloorDarkDisplay : String
caveFloorDarkDisplay =
    "."


caveFloorDarkRead : String
caveFloorDarkRead =
    "~"


caveNSWall : String
caveNSWall =
    "-"


caveWEWall : String
caveWEWall =
    "|"


caveCorridor : String
caveCorridor =
    "#"


caveDoor : String
caveDoor =
    "+"
