module Update.GameInit exposing (update)

import Constant.Game
import Data.GameState
    exposing
        ( GameMode(..)
        , GameState
        )
import Data.Map exposing (addEntity, emptyMap)
import Data.Stremf exposing (strengthToStremf)
import Update.Msg exposing (Msg)
import Utility.Map exposing (prefabMap)


update : Msg -> GameState -> ( GameState, Cmd Msg )
update _ oldState =
    case oldState.gameState of
        GameInit { initialSeed } ->
            let
                entities =
                    [ Constant.Game.playerID ]

                newerMap =
                    addEntity
                        emptyMap
                        Constant.Game.playerID
                        ( 1, 1 )
            in
            ( { gameState =
                    GamePlay
                        { entities = entities
                        , gameMap =
                            { entityMap = newerMap
                            , tileMap = prefabMap
                            , dlevel = 1
                            }
                        , name = "Player"
                        , experience = 0
                        , au = 0
                        , strength = strengthToStremf 10
                        , initialSeed = initialSeed
                        , currentSeed = initialSeed
                        }
              }
            , Cmd.none
            )

        _ ->
            ( oldState, Cmd.none )
