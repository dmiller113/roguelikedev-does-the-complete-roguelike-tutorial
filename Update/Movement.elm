module Update.Movement exposing (update)

import Constant.Game exposing (playerID)
import Data.Collision exposing (Collision, collision)
import Data.GameState
    exposing
        ( GameMode(..)
        , GameState
        )
import Data.Map exposing (moveEntity, whereIs)
import Data.Position exposing (Offset, Position, addPosition)
import Data.RogueState exposing (RogueState)
import Keyboard exposing (Key(..), anyKey)
import Update.Msg exposing (Msg(..))
import Utility.Game exposing (flip)


type Command
    = MovePlayerPosition Position
    | MovePlayerRelativePosition Offset


keyToCommand : Key -> Maybe Command
keyToCommand key =
    case key of
        ArrowUp ->
            Just <| MovePlayerRelativePosition ( 0, -1 )

        ArrowRight ->
            Just <| MovePlayerRelativePosition ( 1, 0 )

        ArrowDown ->
            Just <| MovePlayerRelativePosition ( 0, 1 )

        ArrowLeft ->
            Just <| MovePlayerRelativePosition ( -1, 0 )

        _ ->
            Nothing


moveCommandToNewPosition : Command -> Position -> Position
moveCommandToNewPosition cmd pos =
    case cmd of
        MovePlayerPosition position ->
            position

        MovePlayerRelativePosition offset ->
            addPosition pos offset



-- TODO: swap to taking a Command and map in the update


move :
    RogueState
    -> Maybe Command
    -> Result Collision RogueState
move rogue cmd =
    let
        { gameMap } =
            rogue

        playerPos =
            whereIs gameMap playerID

        newPosition =
            Maybe.map2
                moveCommandToNewPosition
                cmd
                playerPos

        hasCollision =
            collision gameMap
                |> flip Maybe.andThen newPosition
    in
    case hasCollision of
        Nothing ->
            let
                newMap =
                    Maybe.withDefault gameMap <|
                        Maybe.map2
                            (moveEntity gameMap playerID)
                            playerPos
                            newPosition
            in
            Ok { rogue | gameMap = newMap }

        Just c ->
            Err c


update : Msg -> GameState -> ( GameState, Cmd Msg )
update msg state =
    let
        { gameState } =
            state
    in
    case ( gameState, msg ) of
        ( GamePlay rogue, KeyDown key ) ->
            let
                command =
                    Maybe.andThen keyToCommand <| anyKey key

                moveResult =
                    move rogue command

                newRogue =
                    case moveResult of
                        Ok s ->
                            s

                        Err _ ->
                            -- this is where attacking/bumping happens
                            rogue
            in
            ( { gameState = GamePlay newRogue }, Cmd.none )

        _ ->
            ( state, Cmd.none )
