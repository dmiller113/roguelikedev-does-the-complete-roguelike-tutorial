module Update.Msg exposing (Msg(..))

import Keyboard exposing (RawKey(..))


type Msg
    = KeyDown RawKey
