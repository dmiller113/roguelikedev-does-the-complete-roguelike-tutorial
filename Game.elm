module Main exposing (init, main, subscriptions, update, view)

import Browser exposing (element)
import Constant.Game
import Data.Flags exposing (Flags)
import Data.GameState exposing (GameMode(..), GameState)
import Html exposing (div)
import Keyboard exposing (downs)
import Svg exposing (svg)
import Svg.Attributes
    exposing
        ( height
        , preserveAspectRatio
        , viewBox
        , width
        )
import Update.GameInit
import Update.Movement
import Update.Msg exposing (Msg(..))
import Utility.Game exposing (foldGameReducers)
import View.Map exposing (gameMap)
import View.StatusBar as StatusBar


init : Flags -> ( GameState, Cmd Msg )
init { initialSeed } =
    ( { gameState = GameInit { initialSeed = initialSeed }
      }
    , Cmd.none
    )


main : Program Flags GameState Msg
main =
    element
        { update = update
        , view = view
        , init = init
        , subscriptions = subscriptions
        }


update : Msg -> GameState -> ( GameState, Cmd Msg )
update msg state =
    let
        { gameState } =
            state
    in
    case gameState of
        GameInit _ ->
            Update.GameInit.update msg state

        GamePlay _ ->
            let
                initialAcc =
                    ( state, Cmd.none )

                ( newState, cmdMessage ) =
                    List.foldl
                        (foldGameReducers msg)
                        initialAcc
                        [ Update.Movement.update
                        ]
            in
            ( newState
            , Cmd.batch [ cmdMessage ]
            )

        _ ->
            ( state, Cmd.none )


view : GameState -> Html.Html Msg
view state =
    let
        { gameState } =
            state
    in
    case gameState of
        GamePlay rogue ->
            div
                []
                [ svg
                    [ viewBox Constant.Game.viewBoxDimensions
                    , width Constant.Game.svgWidthPx
                    , height Constant.Game.svgHeightPx
                    , preserveAspectRatio "none"
                    ]
                  <|
                    (StatusBar.view rogue :: gameMap rogue)
                ]

        _ ->
            div []
                [ svg
                    [ viewBox Constant.Game.viewBoxDimensions
                    , width Constant.Game.svgWidthPx
                    , height Constant.Game.svgHeightPx
                    , preserveAspectRatio "none"
                    ]
                    []
                ]


subscriptions : GameState -> Sub Msg
subscriptions _ =
    Sub.batch
        [ downs (identity >> KeyDown)
        ]
