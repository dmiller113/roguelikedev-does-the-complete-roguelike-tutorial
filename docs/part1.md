# Roguelike in Elm - Rogue Clone

Hello, and welcome! This is **Part 1** of the roguelikedev subreddit's Roguelike tutorial project.

The tutorial source being used this year can be found at: [Roguelike Tutorials](https://rogueliketutorials.com/tutorials/tcod/v2/)

This year, I'm going to be following the tutorial but instead of using Python with Libtcod as the development language and library, I'm going to be using Elm along with its native Svg rendering (should it prove fast enough) to make a small clone in the vein of the original Rogue.

What that means is the items, enemies, stats, and dungeon layout and objective is being taken from the original Rogue, or at least as close as I can make it from the documentation I can find about it.

If you need help installing **Elm** or what to see how I did it for this project, then refer to [Part 0: Installing Elm](https://gitlab.com/dmiller113/roguelikedev-does-the-complete-roguelike-tutorial/blob/4df7be12be31002afdd0f7c9f92315243a0fcafb/docs/part0.md).

Small caviat: I am not an expert in Elm, and I don't encourage this tutorial as a resource for learning Elm, or functional programming patterns.

So follow along and lets get this game started!

A word on format. When you see a `$` inside of a code section, that is referring to a command you should be typing on your terminal. When you see a `--` followed by a string, that string is simulated output from the command above it. So an example would be
```shell
$ echo Hi
-- Hi
```

## Part 1 - Moving '@'

So the first part of the tutorial concerns itself with getting all the boiler plate of hooking up our libraries and generating our project out the way, and getting to a state that we can show a **'@'** on screen that moves when you push the arrow keys. Seems simple enough, lets give it a go.


#### Making our first Elm file
Now to start development in ernest. Fire up your code editor of choice, I'm using VSCode with its exellent **Elm** plugin, and create a file called `Game.elm`.

**Elm** files need to list the module they're a part of as the first line, so add

```elm
module Main exposing (..)
```

to the top of the file. That states that this file belongs to the `Main` module, and exposes every type and function in it. As this is the base level of our program,
exposing anything likely doesn't matter, but it could so why not. We'll change it later if it makes a difference.

#### Constructing a Model
The first thing I generally do when I try to solve a problem with **Elm** is to try and make a model of the information that my solution is going to need. Our current problem is to display a `@` on the screen and move it in response to inputs on the keyboard corrisponding to the arrow keys.

As this model is likely going to not be kept past this initial tye up phase of the tutorial, I wouldn't suggest taking too long thinking about it, but the model I came up with is this:
```elm
type alias Model =
  { playerGlyph : String
  , playerX : Int
  , playerY : Int
  , playerGlyphColor : String
  }
```

Note, I could have, for clarity, made a couple of additional type aliases

```elm
type alias Color = String

type alias Coord = Int
```

That would allow the type checker to help me in making sure that I am passing in the correct values to functions, and to clarify what I expect functions to take even further. Since this model is not likely to last past this part, I have not done so for the sake of brevity.

So armed with our model of state, we now need to figure some method of displaying it to the user. I plan on using plain svg's to display positionable text, so we'll need to install and import **Elm**'s svg libraries.

#### elm-package.json
To keep track of what packages your **Elm** project needs, **Elm** uses a file called `elm-package.json`. To generate one, we can run the command

```shell
$ yarn elm package install
```

which will create the file for us, and download the basic **Elm** core library.

After that, we can add packages to the `dependencies` key, in the form of

```json
{
  "[elm-package-user]/[package-name]": "[version-start] >= v > [version-end]"
}
```

so for our svg package, it looks like

```json
{
  "elm-lang/svg": "2.0.0 <= v < 3.0.0"
}
```

After adding that, go ahead and run the package install command again. It should, after your confirmation, download the svg package for your project.

From now on, whenever I mention adding a package to this project, this is the method I am referring to, and won't go into detail about again. Because of this, I would make sure that you are familiarized with this before moving on. You also may be able to install packages from the CLI, but that's not the way I tend to do so, so do that at your own risk.

#### View function
Now with a package to handle creating svg tags, lets make a function that handles our output.

We'll need to import the svg package, so do so by

```elm
import Svg exposing (rect, svg, style, text_)
import Svg.Attributes exposing (
  class, dominantBaseline, fill,
  height, viewBox, width, x, y,
  dx, preserveAspectRatio
)
```

We'll also likely want the functions that give Svg tags their attributes, so we import that right under it.

To avoid cluttering our code with `Svg.<thing>`, we **expose** functions we know we're going to work with and that are unlikely to pollute the global namespace with names we'd want to use ourselves or would be unclear.

#### Basic view funciton
A view function at its most basic level has this type signature:
```elm
view : Model -> Html.Html Msg
```
and expresses the relationship that **Elm** enforces in our code: our displayable output is simply a function of our program's state, as expressed in our model. There is no outside influences beyond functions to call that we use to transform our state into its eventual output as **Html**. So our task with this function is figuring out how to take our state, and map it into a single node of **Html**. A good first start is to get our `Svg` tag working.

```elm
view model =
  div
    []
    [ svg
      [ viewBox "0 0 800 240"
      , width "960px"
      , height "624px"
      , preserveAspectRatio "xMinYMin"
      ] []
    ]
```
If you're familiar with `XML` or `JSX` this code likely looks familiar. Elm has a format similar to **React**, but instead of emulating `Html` tags as a separate language ala `JSX`, it uses the same thing that **React** does under the covers: functions.

In this case, it calls a function that produces a **div** component, aptly called `div`, and passes it two lists: a list of `Html.Attribute` and a list of children. 

If you're familar with `JSX`, the equivalent in that would be:
```jsx
view = model => (
  <div>
    <svg 
      viewBox="0 0 800 240"
      width="960px"
      height="624px"
      preserveAspectRatio="xMinYMin"
    />
  </div>
)
```
Looks remarkably similar, doesn't it? But using `JSX` has disadvantages over using plain functions. I can compose and pipe the output of `div` easily, e.g. `div [] [] |> wrapWithDiv`. Its much harder to do that with plain `JSX`, often you must resort to manually creating functions that produce `JSX` that you can then compose and pipe.

Hopefully that comparison between **Elm** display functions and `JSX` helped illustrate some of the power of **Elm** and showed that the fundimentals of **Elm** display have already been introduced into mainstream **Javascript** web development.

#### Helper functions
Now, we could just make a largish view function, that contained all the code and logic needed to draw everything of our, admittedly, small problem, but there are some clear small helper functions that we can make that will help keep the code a bit more readable, at least in my opinion.

The first I can think of is having a function to draw a character onto the svg display. I've titled it `glyph` as all its responsible for is displaying a colored glyph of text. The type annotation for this function is such:

```elm
glyph : String -> Int -> Int -> String -> Svg.Svg Msg
```
Because we're not using more expressive type aliases, its not as clears as it possibly could be, so lets dive into it:

The function takes a letter to display, as a string for ease, though thinking about it now, it would be trivial to correct that to taking a Character with function composition (`>>` in **Elm**). It also takes 2 Ints, representing an x and y **Position** to display the glyph at, along with a **Color** String to use as the color for the glyph. After taking all these, it outputs a **Svg** containing a **Msg**. That type is the return type of most, if not all, of the **Svg** component functions, e.g. **rect**, **circle**, etc.

The actual implementation of this function is not very complex, essentially just being a call to the **text_** component of the `Svg` library, with the parameters passed into the correct spots.
```elm
glyph s px py color =
  text_ [ dominantBaseline "hanging"
    , x <| toString <| px * 10
    , y <| toString <| py * 10
    , class "small"
    , fill color
    ]
    [ Svg.text s ]
```
Something to notice with this function is that **Tag** attributes are not created by simply having key value pairs assigned with the tag, but are instead created by functions coming out of the `Svg.Attribute` module.

#### Tying this into our view function
With this helper function, displaying the players position becomes much easier. By inserting the function call in the child list of the `svg` function, we make a glyph for the player like so:
```elm
view model =
...
  svg 
    [...]
    [ glyph model.playerGlyph model.playerX model.playerY model.playerGlyphColor
    ]
```
With this, our svg has a single child node, our player's glyph, located at the players position, and colored our players color.

...

### Parts to improve
Another thing to notice, is that we're multiplying the position coordinates by a magic number, 10 in this case. The 10 is there to make the positions in terms of glyph sizes, which are 10px tall and wide in the `viewport` of the Svg. We can make this much clearer as to what's happening by replacing them with some sort of constant defined in a module. 

In the next part of this tutorial, part 2, we'll do just that.
