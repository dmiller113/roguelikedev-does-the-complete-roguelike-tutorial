## Part 0: Installing Elm
For this tutorial, obviously we're going to need to get **Elm** installed and accessable. This short part will outline how I did that in this project.
### Installing Elm
This could be very easy, or moderately difficult depending on your Operating System. I'm going to outline the Linux instructions here, which should also work for Mac, as that's what I did. If you're using Windows, I would refer you to the [Elm Homepage](http://www.elm-lang.org).

#### Installing Yarn
I'm going to be installing Elm from the NPM (Node Package Manager) repository, a common library and package manager for javascript development. Elm is on it, as it is a language that compiles (somewhat) down to javascript.

First you're going to need to open your terminal, and execute these commands:
```
$ curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -\necho "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

$ sudo apt update && sudo apt-get install --no-install-recommends yarn
```
If you're uncomfortable running sudo'd commands that you're finding in a random git repo (and you should be), than I would refer you to [Yarn's Install Page](https://yarnpkg.com/en/docs/install)

#### Generating a package.json
You should be able to generate a basic `package.json` from running the command
```
$ yarn init
```
and then answering any questions that it prompts you with.

#### Installing Elm's CLI tools
After generating a `package.json` you can now use yarn to install **elm** and its cli tools. Just run
```shell
$ yarn add elm
```
Wait for everything to be downloaded and install, and you should have **Elm**!
You can verify that you do by running the command
```shell
$ yarn elm -v
-- ...
-- 0.18.0
```
Now, we've installed **Elm** *locally* to our project. It won't pollute your global package space or interfere with other versions of **Elm** you're using for other projects. The trade off is, you need to tack on a `yarn` before everyone of your **Elm** cli commands. I think its a small trade-off.