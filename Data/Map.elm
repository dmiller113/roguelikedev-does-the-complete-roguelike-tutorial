module Data.Map exposing
    ( EntityMap
    , GameMap
    , TileMap
    , addEntity
    , addTile
    , emptyMap
    , emptyTileMap
    , moveEntity
    , whereIs
    )

import Data.Entity exposing (Entity)
import Data.GameTile exposing (GameTile)
import Data.Position exposing (Position)
import Dict
    exposing
        ( Dict(..)
        , empty
        , foldl
        , get
        , insert
        , member
        , remove
        )


type alias EntityMap =
    Dict Position Entity


type alias TileMap =
    Dict Position GameTile


type alias DungeonLevel =
    Int


type alias EntityPositions =
    Dict Entity Position


type alias GameMap =
    { entityMap : EntityMap
    , tileMap : TileMap
    , dlevel : DungeonLevel
    }


emptyMap : EntityMap
emptyMap =
    empty


emptyTileMap : TileMap
emptyTileMap =
    empty


isEmpty : Dict Position v -> Position -> Bool
isEmpty m pos =
    member pos m |> not


byEntity : EntityMap -> EntityPositions
byEntity m =
    let
        foldBy pos entity acc =
            insert entity pos acc
    in
    foldl foldBy empty m


addTile : TileMap -> GameTile -> Position -> TileMap
addTile m tile pos =
    insert pos tile m


addEntity : EntityMap -> Entity -> Position -> EntityMap
addEntity m entity pos =
    insert pos entity m


removeEntity : EntityMap -> Position -> EntityMap
removeEntity m pos =
    remove pos m


whereIs : GameMap -> Entity -> Maybe Position
whereIs { entityMap } entity =
    byEntity entityMap |> get entity


moveEntity : GameMap -> Entity -> Position -> Position -> GameMap
moveEntity gm e oldPosition newPosition =
    let
        { entityMap } =
            gm

        newMap =
            removeEntity entityMap oldPosition
    in
    { gm | entityMap = addEntity newMap e newPosition }
