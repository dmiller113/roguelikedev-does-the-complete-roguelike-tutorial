module Data.InitState exposing (InitState)


type alias InitState =
    { initialSeed : Int
    }
