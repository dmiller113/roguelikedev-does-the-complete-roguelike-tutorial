module Data.Position exposing
    ( Offset
    , Position
    , addPosition
    , fromIndex
    , neighbors
    )

import Constant.Game
    exposing
        ( mapLastHeightIndex
        , mapLastWidthIndex
        , mapWidthGlyph
        )


type alias Offset =
    ( Int, Int )


type alias Position =
    ( Int, Int )


clampPosition : Position -> Position
clampPosition ( x, y ) =
    ( clamp 0 mapLastWidthIndex x
    , clamp 0 mapLastHeightIndex y
    )


addPosition : Position -> Position -> Position
addPosition ( a, b ) ( x, y ) =
    clampPosition ( a + x, b + y )


neighbors : Position -> List Position
neighbors position =
    List.map
        (addPosition position)
        [ ( -1, -1 )
        , ( 0, -1 )
        , ( 1, -1 )
        , ( -1, 0 )
        , ( 1, 0 )
        , ( -1, 1 )
        , ( 0, 1 )
        , ( 1, 1 )
        ]


fromIndex : Int -> Position
fromIndex index =
    ( modBy mapWidthGlyph index, index // mapWidthGlyph )
