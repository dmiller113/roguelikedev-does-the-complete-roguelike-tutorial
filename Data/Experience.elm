module Data.Experience exposing
    ( Experience
    , Level
    , experienceToLevel
    )


type alias Experience =
    Int


type alias Level =
    Int


experienceToLevel : Experience -> Level
experienceToLevel _ =
    1
