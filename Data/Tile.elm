module Data.Tile exposing (Tile)

import Data.Color exposing (Color(..))


type alias Tile =
    { s : String
    , color : Color
    }
