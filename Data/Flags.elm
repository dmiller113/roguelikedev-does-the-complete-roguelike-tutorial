module Data.Flags exposing (Flags)


type alias Flags =
    { initialSeed : Int }
