module Data.Collision exposing
    ( Collision
    , collision
    )

import Data.Entity exposing (Entity)
import Data.GameTile exposing (isBlocking)
import Data.Map exposing (GameMap)
import Data.Position exposing (Position)
import Dict exposing (get)


type Collision
    = EntityCollision Entity
    | TileCollision


collision : GameMap -> Position -> Maybe Collision
collision { entityMap, tileMap } position =
    let
        entityCollision =
            get position entityMap
                |> Maybe.map EntityCollision
    in
    case entityCollision of
        Nothing ->
            case get position tileMap of
                Just tile ->
                    if isBlocking tile then
                        Just TileCollision

                    else
                        Nothing

                Nothing ->
                    Just TileCollision

        _ ->
            entityCollision
