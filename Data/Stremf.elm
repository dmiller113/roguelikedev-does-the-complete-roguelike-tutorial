module Data.Stremf exposing
    ( Stremf
    , changeStremf
    , strengthToStremf
    , strengthToString
    )

import String exposing (fromInt)
import Utility.String exposing (parens)


type alias Strength =
    Int


type alias ToHitBonus =
    Int


type alias DamageBonus =
    Int


type Stremf
    = Stremf Strength Strength ToHitBonus DamageBonus


strengthToStremf : Strength -> Stremf
strengthToStremf str =
    Stremf str str (strToHit str) (strToDamage str)


strToDamage : Strength -> DamageBonus
strToDamage str =
    let
        lessThan7 =
            str < 7

        greaterThan22 =
            str > 21

        is31 =
            str == 31
    in
    case ( lessThan7, greaterThan22, is31 ) of
        ( True, False, False ) ->
            str - 7

        ( False, True, False ) ->
            5

        ( False, True, True ) ->
            6

        _ ->
            if str < 16 then
                0

            else if str < 17 then
                1

            else if str == 18 then
                2

            else if str < 21 then
                3

            else
                4


strToHit : Strength -> ToHitBonus
strToHit str =
    let
        is31 =
            str == 31

        greater20 =
            str > 20

        less7 =
            str < 7
    in
    case ( less7, greater20, is31 ) of
        ( True, False, False ) ->
            str - 7

        ( False, True, False ) ->
            3

        ( False, True, True ) ->
            4

        _ ->
            if str < 17 then
                0

            else if str < 19 then
                1

            else
                2


changeStremf : Stremf -> Int -> Stremf
changeStremf stremf dChange =
    case stremf of
        Stremf curStr maxStr _ _ ->
            let
                newStr =
                    curStr + dChange

                newMax =
                    max newStr maxStr
            in
            Stremf newStr newMax (strToHit newStr) (strToDamage newStr)


strengthToString : Stremf -> String
strengthToString stremf =
    case stremf of
        Stremf curStr maxStr _ _ ->
            "Str:" ++ fromInt curStr ++ parens maxStr ++ "  "
