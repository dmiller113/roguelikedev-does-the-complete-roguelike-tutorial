module Data.RogueState exposing (RogueState)

import Data.Aurum exposing (Aurum)
import Data.Entity exposing (Entity)
import Data.Experience exposing (Experience)
import Data.Map exposing (GameMap)
import Data.Stremf exposing (Stremf)


type alias RogueState =
    { entities : List Entity
    , gameMap : GameMap
    , name : String
    , experience : Experience
    , au : Aurum
    , strength : Stremf
    , initialSeed : Int
    , currentSeed : Int
    }
