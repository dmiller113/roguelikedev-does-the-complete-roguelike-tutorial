module Data.GameState exposing (GameMode(..), GameState)

import Data.InitState exposing (InitState)
import Data.RogueState exposing (RogueState)


type GameMode
    = GameInit InitState
    | GameStart
    | GamePlay RogueState


type alias GameState =
    { gameState : GameMode
    }
