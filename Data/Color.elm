module Data.Color exposing
    ( Color(..)
    , ColorCode(..)
    , ColorIntensity(..)
    , IntensityHexes
    , codeToHexes
    , colorToHex
    , intensityHexes
    , intensityToHexCode
    )

import Utility.Game exposing (flip)


type ColorCode
    = Black
    | Red
    | Green
    | Yellow
    | Blue
    | Purple
    | Cyan
    | White


type ColorIntensity
    = Bold
    | Regular


type Color
    = Color ColorCode ColorIntensity


type alias IntensityHexes =
    { regular : String
    , bold : String
    }


intensityHexes : String -> String -> IntensityHexes
intensityHexes regular bold =
    { regular = regular, bold = bold }


codeToHexes : ColorCode -> IntensityHexes
codeToHexes code =
    case code of
        Black ->
            intensityHexes "#000000" "#686868"

        Red ->
            intensityHexes "#c91b00" "#ff6e67"

        Green ->
            intensityHexes "#00c200" "#5ffa68"

        Yellow ->
            intensityHexes "#c7c400" "#fffc67"

        Blue ->
            intensityHexes "#0225c7" "#6871ff"

        Purple ->
            intensityHexes "#ca30c7" "#ff77ff"

        Cyan ->
            intensityHexes "#00c5c7" "#60fdff"

        White ->
            intensityHexes "#c7c7c7" "#FFFFFF"


intensityToHexCode : IntensityHexes -> ColorIntensity -> String
intensityToHexCode { bold, regular } intensity =
    case intensity of
        Bold ->
            bold

        Regular ->
            regular


colorToHex : Color -> String
colorToHex color =
    case color of
        Color code intensity ->
            (codeToHexes >> flip intensityToHexCode intensity) code
