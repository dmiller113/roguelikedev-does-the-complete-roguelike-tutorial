module Data.Glyph exposing
    ( Glyph
    , fromGameTile
    )

import Data.GameTile exposing (GameTile)
import Data.Position exposing (Position)


type alias Glyph =
    { tile : GameTile
    , position : Position
    }


fromGameTile : GameTile -> Position -> Glyph
fromGameTile tile pos =
    { tile = tile, position = pos }
