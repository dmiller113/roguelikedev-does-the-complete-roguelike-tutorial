module Data.GameTile exposing
    ( GameTile(..)
    , caveWall
    , fromString
    , isBlocking
    , isDark
    , isSeeThrough
    , tileFromGameTile
    )

import Constant.Actor.Tile as ActorTile
import Constant.Cave.Tile as CaveTile
import Data.Tile exposing (Tile)


type GameTile
    = CaveFloor
    | CaveFloorDark
    | CaveNSWall
    | CaveWEWall
    | CaveCorridor
    | CaveDoor
    | Ant
    | Bat
    | Centar
    | Dragon
    | FloatingEye
    | VenusFlytrap
    | Gnome
    | Hobgoblin
    | InvisibleStalker
    | Jackal
    | Kobold
    | Leprechaun
    | Mimic
    | Nymph
    | Orc
    | Quasit
    | PurpleWorm
    | RustMonster
    | Snake
    | Troll
    | UmberHulk
    | Vampire
    | Wraith
    | Xorn
    | Yeti
    | Zombie
    | Player


tileFromGameTile : GameTile -> Tile
tileFromGameTile gt =
    case gt of
        CaveFloor ->
            CaveTile.caveFloor

        CaveFloorDark ->
            CaveTile.caveFloor

        CaveNSWall ->
            CaveTile.caveNSWall

        CaveWEWall ->
            CaveTile.caveWEWall

        CaveCorridor ->
            CaveTile.caveCorridor

        CaveDoor ->
            CaveTile.caveDoor

        _ ->
            ActorTile.player



-- Useful for reading a test map


fromString : String -> Maybe GameTile
fromString s =
    case s of
        "." ->
            Just CaveFloor

        "~" ->
            Just CaveFloorDark

        "-" ->
            Just CaveNSWall

        "|" ->
            Just CaveWEWall

        "+" ->
            Just CaveDoor

        "#" ->
            Just CaveCorridor

        _ ->
            Nothing


caveWall : Bool -> GameTile
caveWall isNS =
    if isNS then
        CaveNSWall

    else
        CaveWEWall


isDark : GameTile -> Bool
isDark t =
    case t of
        CaveFloorDark ->
            True

        CaveCorridor ->
            True

        _ ->
            False


isBlocking : GameTile -> Bool
isBlocking t =
    case t of
        CaveNSWall ->
            True

        CaveWEWall ->
            True

        _ ->
            False


isSeeThrough : GameTile -> Bool
isSeeThrough t =
    case t of
        CaveFloor ->
            True

        _ ->
            False
